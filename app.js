const path = require('path');
const moment = require('moment');

const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const app = express();
const router = require('./routes');

app.use(express.static(path.join(__dirname, 'client/build')))
    .use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Methods', 'POST, PUT, OPTIONS, DELETE, GET');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        next();
    })
    .use(morgan((tokens, req, res) => [
        `[${moment().format('HH:mm:ss')}]`,
        tokens.method(req, res),
        tokens.url(req, res),
        tokens.status(req, res),
        tokens.res(req, res, 'content-length'),
        '-',
        tokens['response-time'](req, res), 'ms'
    ].join(' ')))
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({ extended: false }))
    .use('/api', router)
    .use((req, res) => {
        res.sendFile(path.join(
            __dirname + '/client/build/index.html'
        ));
    });

const port = process.env.PORT || 80;
app.listen(port);

console.log(`Youtube to mp3 started on http://localhost:${port}`);