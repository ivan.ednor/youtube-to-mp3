import { connect } from 'react-redux';
import Catalog from '../../components/Catalog';
import { getAllRecords } from '../../api';

const mapStateToProps = state => ({
	records: state.music.records,
	count: state.music.countOfRecordsOnServer
});

const mapDispatchToProps = dispatch => ({
	getAllRecords
});

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);