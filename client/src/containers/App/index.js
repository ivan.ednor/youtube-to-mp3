import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { toggleBgColor } from '../../store/actions/view';
import App from '../../components/App';

const mapStateToProps = state => ({
	bgColor: state.view.mainBgColor
});

const mapDispatchToProps = dispatch => ({
	toggleBgColor: () => {
		dispatch(
			toggleBgColor()
		);
	}
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));