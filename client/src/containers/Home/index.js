import { connect } from 'react-redux';
import _ from 'lodash';
import { pushRecord } from '../../store/actions/music';
import Home from '../../components/Home';
import { getConvertedAudio } from '../../api';

const mapStateToProps = state => ({
	userRecords: state.music.userRecords
});

const mapDispatchToProps = dispatch => ({
	getRecord: async ({ url, filePath }) => {
		if(filePath) {
			dispatch(
				pushRecord(filePath)
			);

			return null;
		}

		const data = await getConvertedAudio(url);
		const file = _.get(data, 'file');

		if(file) {
			dispatch(
				pushRecord(`${file}.mp3`)
			);
		}
	}
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);