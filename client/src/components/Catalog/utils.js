import _ from 'lodash';

export const formatText = text => _.lowerCase(
    _.trim(text)
);