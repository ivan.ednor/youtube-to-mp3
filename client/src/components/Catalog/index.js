import _ from 'lodash';
import React, { Component } from 'react';
import { Input } from 'reactstrap';
import DocumentTitle from 'react-document-title';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';
import RecordsList from '../RecordsList';
import { formatText } from './utils';
import './index.css';

export default class Catalog extends Component {
    static propTypes = {
        records: PropTypes.arrayOf(
            PropTypes.string.isRequired
        ).isRequired,
        count: PropTypes.number.isRequired,
        getAllRecords: PropTypes.func.isRequired
    };

    state = {
        search: '',
        pagination: {
            limit: 50,
            offset: 0
        }
    };

    onSearchChange = e => {
        const {  value } = e.target;

        this.setState({
            search: value
        }, () => {
            this.search(value);
        });
    };

    search = _.debounce((search) => {
        const { getAllRecords } = this.props;
        const { pagination: { limit }} = this.state;

        this.setState({
            pagination: {
                limit,
                offset: 0
            }
        }, () => {
            getAllRecords(limit, 0, formatText(search));
        });
    }, 250);

    handlePageClick = ({ selected }) => {
        const { pagination: { limit }} = this.state;

        this.setState({
            pagination: {
                limit,
                offset: selected * limit
            }
        });
    };

    componentDidMount() {
        const { getAllRecords } = this.props;
        const { pagination: { limit, offset }} = this.state;

        getAllRecords(limit, offset);
    }

    componentDidUpdate(prevProps, prevState) {
        const { getAllRecords } = this.props;
        const { pagination } = this.state;
        const { pagination: prevPagination } = prevState;

        if(!_.isEqual(pagination, prevPagination)) {
            const { limit, offset } = pagination;

            getAllRecords(limit, offset);
        }
    }

    render() {
        const { records, count } = this.props;
        const { search, pagination: { limit, offset }} = this.state;
        const pagesCount = Math.ceil(count / limit);
        const curPage = ((offset + limit) / limit) - 1;

        return (
            <DocumentTitle title='Catalog'>
                <div className='catalog'>
                    <Input placeholder='Search...' onChange={this.onSearchChange} value={search} />
                    <ReactPaginate
                        forcePage={curPage}
                        previousLabel={'previous'}
                        nextLabel={'next'}
                        breakLabel={'...'}
                        breakClassName={'break-me'}
                        pageCount={pagesCount}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={5}
                        onPageChange={this.handlePageClick}
                        containerClassName={'pagination'}
                        subContainerClassName={'pages pagination'}
                        activeClassName={'active'}
                    />
                    <RecordsList records={records} />
                </div>
            </DocumentTitle>
        );
    }
}