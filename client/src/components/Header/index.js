import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import './index.css';

class Header extends Component {
    state = {
        selectedIndex: 0
    };

    select = (selectedIndex) => this.setState({ selectedIndex });
    
    render() {
        return (
            <div className='pt-5 pb-2 header'>
                <Link to='/'>
                    <Button>Home</Button>
                </Link>
                <Link to='/catalog'>
                    <Button>Catalog</Button>
                </Link>
            </div>
        );
    }
}

export default Header;