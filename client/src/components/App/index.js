import React, { Component } from 'react';
import classNames from 'classnames';
import { Switch, Route } from 'react-router-dom';
import ReactSwitch from 'react-switch';
import PropTypes from 'prop-types';
import Home from '../../containers/Home';
import Catalog from '../../containers/Catalog';
import Header from '../Header';
import './index.css';

export default class App extends Component {
	static propTypes = {
		bgColor: PropTypes.string.isRequired,
		toggleBgColor: PropTypes.func.isRequired
	};

	render() {
		const { bgColor, toggleBgColor } = this.props;
		const isDarkTheme = bgColor !== '#ffffff';

		return (
			<div style={{backgroundColor: bgColor}} className={classNames(
				'bg-color-container pl-5',
				{ 'dark-theme': isDarkTheme }
			)}>
				<Header />
				<ReactSwitch onChange={toggleBgColor} checked={isDarkTheme} />
				<div className='pt-2'>
					<Switch>
						<Route exact path='/' component={Home} />
						<Route path='/catalog' component={Catalog} />
					</Switch>
				</div>
			</div>
		);
	}
}