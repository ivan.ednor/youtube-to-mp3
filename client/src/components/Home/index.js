import _ from 'lodash';
import React, { Component } from 'react';
import validUrl from 'valid-url';
import PropTypes from 'prop-types';
import { Input, Button } from 'reactstrap';
import { ClipLoader } from 'react-spinners';
import DocumentTitle from 'react-document-title';
import RecordsList from '../RecordsList';
import './index.css';

class Home extends Component {
	static propTypes = {
        userRecords: PropTypes.arrayOf(
        	PropTypes.string.isRequired
		).isRequired,
        getRecord: PropTypes.func.isRequired
	};

	state = {
		url: '',
        isInProgress: false
	};

	componentDidMount() {
		const indexOfMp3InUrl = window.location.href.indexOf('/mp3');
		if(indexOfMp3InUrl > -1) {
			const { getRecord } = this.props;

			getRecord({
				file: decodeURI(window.location.href.slice(indexOfMp3InUrl))
			});
		}
	}

	onChange = (e) => {
		this.setState({
			url: e.target.value
		});
	};

	isUrl = (url) => validUrl.isUri(url);

	onSubmit = async (e) => {
		e.preventDefault();
		if(this.isUrl(this.state.url)) {
			const { getRecord } = this.props;

            this.setState({
                isInProgress: true
			});
			
			await getRecord({
				url: this.state.url
			});

            this.setState({
                isInProgress: false,
                url: ''
            });
		}
	};

    render() {
		const { url, isInProgress } = this.state;
		const { userRecords } = this.props;

        return (
			<DocumentTitle title='Home'>
				<div className='app'>
					<form className='form' onSubmit={this.onSubmit}>
						<p>Input url of video to get mp3</p>
						<div className='d-inline-flex align-items-center'>
							<Input
								placeholder='Url'
								onChange={this.onChange}
								value={url}
							/>
							<Button onClick={this.onSubmit}
								disabled={isInProgress || !this.isUrl(url)}>
								Get mp3
							</Button>
						</div>
					</form>
					{isInProgress && (
						<div className='d-flex position-relative align-items-center mt-2'>
							<p className='text-nowrap pb-0 pr-2'>Please, wait</p>
							<ClipLoader
								sizeUnit='px'
								size={25}
								color='#00b8ff'
								loading={true} />
						</div>
					)}
					<RecordsList records={_.cloneDeep(userRecords)} />
				</div>
			</DocumentTitle>
        );
    }
}

export default Home;