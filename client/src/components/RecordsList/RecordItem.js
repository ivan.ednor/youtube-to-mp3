import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class RecordItem extends Component {
    static propTypes = {
        url: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired
    };

    render() {
        const { url, name } = this.props;

        return (
            <div>
                <p>{name}</p>
                <audio controls>
                    <source src={url} type='audio/ogg' />
                </audio>
                <p>You also can get audio here <a href={url} target='_blank'>Download</a></p>
            </div>
        );
    }
}
