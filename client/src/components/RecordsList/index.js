import React, { Component } from 'react';
import _ from 'lodash';
import RecordItem from './RecordItem';
import PropTypes from 'prop-types'

export default class RecordsList extends Component {
    static propTypes = {
        records: PropTypes.arrayOf(
            PropTypes.string
        )
    };

    shouldComponentUpdate(nextProps) {
        return !_.isEqual(nextProps.records, this.props.records);
    }

    render() {
        const { records } = this.props;
        const transformedRecords = _.map(records, record => ({
            name: record,
            key: `record_${_.uniqueId()}`,
            url: `/mp3/${record}`
        }));

        return _.map(transformedRecords, record => <RecordItem {...record} />);
    }
}