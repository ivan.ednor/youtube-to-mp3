export const post = (url, body) => new Promise((resolve, reject) => {
    let data;

    fetch(url, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        body: JSON.stringify(body)
    }).catch(err => {
        reject(err);
    }).then(response => {
        data = response;
        return response.json();
    })
    .catch(() => {
        return data;
    }).then(data => {
        resolve(data);
    });
});

export const setBgColorInLocalStorage = color => {
    localStorage.setItem('bgColor', color);
};

export const getBgColorFromLocalStorage = () => {
    return localStorage.getItem('bgColor');
};