export const SET_RECORDS = 'SET_RECORDS';
export const PUSH_USER_RECORD = 'PUSH_USER_RECORD';

export const setRecords = (records, count) => ({
    type: SET_RECORDS,
    payload: {
        records,
        count
    }
});

export const pushRecord = record => ({
    type: PUSH_USER_RECORD,
    payload: record
});