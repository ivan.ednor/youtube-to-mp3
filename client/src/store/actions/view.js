export const SET_BG_COLOR = 'SET_BG_COLOR';
export const TOGGLE_BG_COLOR = 'TOGGLE_BG_COLOR';

export const setBgColor = color => ({
    type: SET_BG_COLOR,
    payload: color
});

export const toggleBgColor = () => ({
    type: TOGGLE_BG_COLOR
});