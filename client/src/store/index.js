import { createStore } from 'redux';
import rootReducer from './reducers';
import storeInit from './storeInit';

const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

storeInit(store.dispatch);

export default store;