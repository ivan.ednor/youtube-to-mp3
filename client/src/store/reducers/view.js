import { SET_BG_COLOR, TOGGLE_BG_COLOR } from '../actions/view';
import { setBgColorInLocalStorage } from '../../utils';

const initialState = {
    mainBgColor: '#ffffff'
};

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch(type) {
        case SET_BG_COLOR: {
            setBgColorInLocalStorage(payload);

            return {
                ...state,
                mainBgColor: payload
            };
        }
        case TOGGLE_BG_COLOR: {
            const color = state.mainBgColor === '#ffffff'?
            '#000000': '#ffffff'

            setBgColorInLocalStorage(color);

            return {
                ...state,
                mainBgColor: color
            };
        }
        default: {
            return state;
        }
    }
}