import { PUSH_USER_RECORD, SET_RECORDS } from '../actions/music';
import _ from 'lodash';

const initialState = {
    records: [],
    countOfRecordsOnServer: 0,
    userRecords: []
};

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch(type) {
        case PUSH_USER_RECORD: {
            const { userRecords } = state;

            if (!_.includes(userRecords, payload)) {
                userRecords.push(payload);
            }

            return {
                ...state,
                userRecords
            };
        }
        case SET_RECORDS: {
            const { records, count } = payload;

            return {
                ...state,
                records: _.isArray(records)? records: state.records,
                countOfRecordsOnServer: count
            };
        }
        default: {
            return state;
        }
    }
}