import { combineReducers } from 'redux';
import musicReducer from './music';
import viewReducer from './view';

export default combineReducers({
    music: musicReducer,
    view: viewReducer
});