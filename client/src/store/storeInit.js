import _ from 'lodash';
import { setBgColor } from './actions/view';
import { getBgColorFromLocalStorage } from '../utils';

export const setBgColorInState = (dispatch, color) => {
    dispatch(
        setBgColor(color)
    );
};

export default dispatch => {
    const color = getBgColorFromLocalStorage();

    if(!_.isNull(color)) {
        setBgColorInState(dispatch, color);
    }
}