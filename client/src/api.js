import { post } from './utils';
import store from './store';
import { setRecords } from './store/actions/music';

export const getAllRecords = async (limit, offset, search) => {
    const { files, count } = await post('/api/music/all', { limit, offset, search });

    store.dispatch(
        setRecords(files, count)
    );
};

export const getConvertedAudio = url => post('/api/music/convert', { url });