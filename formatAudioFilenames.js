const _ = require('lodash');
const slug = require('slug');
const fs = require('fs');
const { getAllRecordsFromDir } = require('./routes/utils');

(async () => {
    const musicPath = `${__dirname}/client/build/mp3`;
    const records = await getAllRecordsFromDir(musicPath, 0, Number.MAX_SAFE_INTEGER);

    _.forEach(records, (file) => {
        const newFilename = `${slug(
            _.chain(file)
                .split('.mp3')
                .first()
                .value()
        )}.mp3`;
        
        if(file !== newFilename) {
            try {
                fs.renameSync(
                    `${musicPath}/${file}`,
                    `${musicPath}/${newFilename}`
                );
            } catch(err) {
                console.log(err);
            }
        }
    });
})();