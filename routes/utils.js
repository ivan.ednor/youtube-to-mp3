const _ = require('lodash');
const fs = require('fs');

const formatText = text => _.lowerCase(
    _.trim(text)
);

const getAllRecordsFromDir = (musicPath, offset = 0, limit = 50, search = '') => new Promise((resolve, reject) => {
    try {
        fs.readdir(musicPath, (err, files) => {
            if(err) {
                reject(err);
            } else {
                resolve(
                    _.chain(files)
                        .filter((file) =>
                            _.includes(file, '.mp3') && _.includes(formatText(file), search)
                        ).reduce((acc, file, index) => {
                            if(index >= offset && index < offset + limit) {
                                acc.push(file);
                            }

                            return acc;
                        }, []).value()
                );
            }
        });
    } catch(err) {
        console.log(err);
        return [];
    }
});

const getCountOfRecords = (musicPath, search = '') => new Promise((resolve, reject) => {
    try {
        fs.readdir(musicPath, (err, files) => {
            if(err) {
                reject(err);
            } else {
                resolve(
                    _.filter(files, (file) =>
                        _.includes(file, '.mp3') && _.includes(formatText(file), search)
                    ).length
                );
            }
        });
    } catch(err) {
        console.log(err);
        return 0;
    }
});

module.exports = {
    getAllRecordsFromDir,
    getCountOfRecords
};