const fs = require('fs');
const slug = require('slug');
const _ = require('lodash');
const { getAllRecordsFromDir, getCountOfRecords } = require('./utils');

const express = require('express');
const router = express.Router();

const extractAudio = require('ffmpeg-extract-audio');
const ytdl = require('ytdl-core');
const getYouTubeID = require('get-youtube-id');
const getYoutubeTitle = require('get-youtube-title');

const musicPath = (() => {
    const tmp = __dirname.split('/');

    tmp.pop();

    return `${tmp.join('/')}/client/build/mp3`;
})();

router.post('/convert', (req, res) => {
    try {
        const { url } = req.body;
        const youtubeId = getYouTubeID(url);

        if(_.isNull(youtubeId)) {
            res.json({
                success: false,
                err: `It's not a youtube video!`
            });

            return null;
        }

        getYoutubeTitle(youtubeId, async (err, title) => {
            title = slug(title);

            if(err) {
                res.json({
                    success: false,
                    err
                });

                return null;
            }

            const files = await getAllRecordsFromDir(musicPath);
            const futureFileName = `${title}.mp3`;
            const video = `/tmp/${title}.mp4`;
            const audio = `${musicPath}/${futureFileName}`;
            const isNew = !_.find(files, file => file === futureFileName);

            if(!isNew) {
                res.json({
                    success: true,
                    file: title
                });

                return null;
            }

            ytdl(url).pipe(fs.createWriteStream(video)).on('finish', async () => {
                await extractAudio({
                    input: video,
                    output: audio
                });

                fs.unlink(video, (err) => {
                    if(err) {
                        res.json({
                            success: false,
                            err
                        });

                        return null;
                    };

                    res.json({
                        success: true,
                        file: title
                    });
                });
            });
        });
    } catch(err) {
        console.log('error', err);
        res.json({
            success: false,
            err
        });
    }
});

router.post('/all', async (req, res) => {
    try {
        const {
            limit,
            offset,
            search
        } = req.body;
        const files = await getAllRecordsFromDir(musicPath, offset, limit, search);

        res.json({
            success: true,
            files,
            count: await getCountOfRecords(musicPath, search)
        });
    } catch(err) {
        console.log(err);
        res.json({
            success: false,
            err
        });
    }
});

module.exports = router;