FROM node:carbon-alpine

WORKDIR /home/youtube-to-mp3

COPY . .

RUN npm install && \
    cd client && \
    npm install && \
    npm run build && \
    rm -rf node_modules public src .gitignore package-lock.json package.json && \
    cd .. && \
    apk add --update ffmpeg

EXPOSE 5555

CMD sh -c "npm run prod"